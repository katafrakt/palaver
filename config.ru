# frozen_string_literal: true

require "rack/static"
use Rack::Static,
  urls: ["/assets"],
  root: "public",
  header_rules: [
    ["/assets", {"Cache-Control" => "public, max-age=31536000"}]
  ]

require "rack/method_override"
use Rack::MethodOverride

require_relative "lib/auth/warden"
Auth::Warden.init(self)

require "hanami/boot"
run Hanami.app
