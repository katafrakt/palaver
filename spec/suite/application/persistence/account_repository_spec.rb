# frozen_string_literal: true

RSpec.describe Palaver::Persistence::Repositories::Account, :db do
  subject { described_class.new }

  describe "#authenticate" do
    before do
      subject.create(email: "abc@mail.com", password: "test123")
    end

    it "authenticates with correct user and password" do
      account = subject.authenticate(email: "abc@mail.com", password: "test123")
      expect(account).to be_kind_of(Palaver::Persistence::Relations::Account)
      expect(account.email).to eq("abc@mail.com")
    end

    it "returns nil when cannot authenticate" do
      expect(subject.authenticate(email: "abc@mail.com", password: "123test")).to be_nil
    end
  end
end
