# frozen_string_literal: true

require "argon2"

module Palaver
  module Persistence
    module Repositories
      class Account < Palaver::Repository[:accounts]
        def create(email:, password:)
          payload = {
            email: email,
            password: Argon2::Password.new.create(password),
            created_at: Time.now
          }
          accounts.command(:create).call(payload)
        end

        def authenticate(email:, password:)
          account = accounts.where(email: email).first

          account if account && Argon2::Password.verify_password(password, account.password)
        end
      end
    end
  end
end
