# frozen_string_literal: true

module Palaver
  module Persistence
    module Relations
      class Accounts < ROM::Relation[:sql]
        schema(:accounts) do
          attribute :id, Types::Integer
          attribute :email, Types::String
          attribute :password, Types::String
          attribute :created_at, Types::Time
        end
      end
    end
  end
end
