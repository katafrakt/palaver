# auto_register: false
# frozen_string_literal: true

module Palaver
  module Entities
  end
end

Dir[File.join(__dir__, "entities", "*.rb")].sort.each(&method(:require))
