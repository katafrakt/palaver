# frozen_string_literal: true

require "argon2"

Warden::Strategies.add(:password) do
  def valid?
    params["email"] || params["password"]
  end

  def authenticate!
    account = Palaver::Persistence::Repository::Account.authenticate(params["email"], params["password"])
    account.nil? ? fail!("Could not log in") : success!(u)
  end
end
