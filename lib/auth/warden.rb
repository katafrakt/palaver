# frozen_string_literal: true

require "warden"
require_relative "strategies/password"

module Auth
  module Warden
    def self.init(context)
      context.use ::Warden::Manager do |manager|
        manager.default_strategies :password
      end
    end
  end
end
