# frozen_string_literal: true

# Seed data for the application
#
# The command `bin/hanami db seed` executes these seeds
Palaver::Persistence::Repositories::Account.new.create(email: "test@example.com", password: "password")
