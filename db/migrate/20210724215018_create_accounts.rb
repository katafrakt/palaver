# frozen_string_literal: true

ROM::SQL.migration do
  change do
    create_table(:accounts) do
      primary_key :id
      String :email, null: false
      String :password, null: false
      DateTime :created_at, null: false
      index :email, unique: true
    end
  end
end
