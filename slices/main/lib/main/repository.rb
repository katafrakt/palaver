# frozen_string_literal: true

require "palaver/repository"
require_relative "entities"

module Main
  class Repository < Palaver::Repository
    struct_namespace Entities
  end
end
