# frozen_string_literal: true

begin
  require "break"
rescue LoadError => e
  raise unless e.path == "break"
end

require "hanami"

module Palaver
  class Application < Hanami::Application
    config.sessions = :cookie, {
      key: "palaver.session",
      secret: settings.session_secret,
      expire_after: 60 * 60 * 24 * 365 # 1 year
    }

    config.logger = {
      level: :debug,
      stream: settings.log_to_stdout ? $stdout : "log/#{Hanami.env}.log"
    }

    assets_server = settings.assets_server_url
    config.actions.default_headers["Content-Security-Policy"] = \
      "base-uri 'self'; " \
      "child-src 'self'; " \
      "connect-src 'self'; " \
      "default-src 'none'; " \
      "font-src 'self'; " \
      "form-action 'self'; " \
      "frame-ancestors 'self'; " \
      "frame-src 'self'; " \
      "img-src 'self' https: data:; " \
      "media-src 'self'; " \
      "object-src 'none'; " \
      "plugin-types application/pdf; " \
      "script-src 'self' #{assets_server}; " \
      "style-src 'self' 'unsafe-inline' https: #{assets_server}"
  end
end
